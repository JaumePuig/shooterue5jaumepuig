// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyChar.h"

#include "PlayerChar.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Components/BoxComponent.h"

// Sets default values
AEnemyChar::AEnemyChar()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	hitZone = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxColl"));
	hitZone->SetupAttachment(RootComponent);

	sightConf = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConf"));
	sightConf->SightRadius = BIG_NUMBER;
	sightConf->LoseSightRadius = BIG_NUMBER;
	sightConf->PeripheralVisionAngleDegrees = 360.f;
	sightConf->DetectionByAffiliation.bDetectEnemies = true;
	sightConf->DetectionByAffiliation.bDetectFriendlies = true;
	sightConf->DetectionByAffiliation.bDetectNeutrals = true;
	sightConf->SetMaxAge(.1);

	AIPerception = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	AIPerception->ConfigureSense(*sightConf);
	AIPerception->SetDominantSense(sightConf->GetSenseImplementation());
	AIPerception->OnPerceptionUpdated.AddDynamic(this, &AEnemyChar::OnSensed);

	currVel = FVector::ZeroVector;
	moveSpeed = 5.f;
	distanceSQRT = BIG_NUMBER;

}

// Called when the game starts or when spawned
void AEnemyChar::BeginPlay()
{
	Super::BeginPlay();

	currentHealth = maxHealth;

	basePos = GetActorLocation();
	
}

// Called every frame
void AEnemyChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!currVel.IsZero()) {
		newPos = GetActorLocation() + currVel + DeltaTime;

		if (backToBase) {
			double sqrt = (newPos - basePos).SizeSquared2D();
			if (sqrt < distanceSQRT) {
				distanceSQRT = sqrt;
			}
			else {
				currVel = FVector::ZeroVector;
				distanceSQRT = BIG_NUMBER;
				backToBase = false;

				SetNewRotation(GetActorForwardVector(), GetActorLocation());
			}
		}

		SetActorLocation(newPos);
	}

}

// Called to bind functionality to input
void AEnemyChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if(IsValid(hitZone))
		hitZone->OnComponentBeginOverlap.AddDynamic(this, &AEnemyChar::OnHit);

}

void AEnemyChar::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit) {

	APlayerChar* player = Cast<APlayerChar>(OtherActor);
	if (player) {
		//player.Hit(dmg);
	}

}

void AEnemyChar::OnSensed(const TArray<AActor*>& upActors) {

	for (const auto& act : upActors) {

		APlayerChar* player = Cast<APlayerChar>(act);

		if (player) {

			FActorPerceptionBlueprintInfo info;
			AIPerception->GetActorsPerception(act, info);

			FVector dir;
			if (info.LastSensedStimuli[0].WasSuccessfullySensed()) {
				dir = act->GetActorLocation() - GetActorLocation();
				dir.Z = 0;

				currVel = dir.GetSafeNormal() * moveSpeed;

				SetNewRotation(act->GetActorLocation(), GetActorLocation());
			}
			else {
				/*dir = basePos - GetActorLocation();
				dir.Z = 0;

				if (dir.SizeSquared2D() > 1.) {
					currVel = dir.GetSafeNormal2D() * moveSpeed;
					backToBase = true;

					SetNewRotation(basePos, GetActorLocation());
				}*/

			}

		}

	}

}

void AEnemyChar::SetNewRotation(FVector target, FVector curr) {
	FVector newDir = target - curr;
	newDir.Z = 0.f;

	enemyRot = newDir.Rotation();

	SetActorRotation(enemyRot);
}

void AEnemyChar::GetDamaged(float damage) {
	
	currentHealth -= damage;

	UE_LOG(LogTemp, Warning, TEXT("GetDamaged"));

	if (currentHealth <= 0) {

		Dieded();

	}

}

void AEnemyChar::Dieded() {

	Destroy();

}
