// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyChar.generated.h"

UCLASS()
class INCLASSSHOOTER_API AEnemyChar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyChar();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		float maxHealth = 5;
		float currentHealth = 5;
	UPROPERTY(EditAnywhere)
		class UBoxComponent* hitZone;

	UPROPERTY(VisibleAnywhere, Category=EnemyMovement)
		FRotator enemyRot;
	UPROPERTY(VisibleAnywhere, Category=EnemyMovement)
		FVector currVel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=EnemyMovement)
		float moveSpeed;
	UPROPERTY(VisibleAnywhere, Category=EnemyMovement)
		class UAIPerceptionComponent* AIPerception;
	UPROPERTY(VisibleAnywhere, Category=EnemyMovement)
		class UAISenseConfig_Sight* sightConf;

	
	UPROPERTY(VisibleAnywhere, Category=EnemyValues)
		float health = 100.f;
	UPROPERTY(VisibleAnywhere, Category=EnemyValues)
		FVector basePos;

	FVector newPos;
	float distanceSQRT;
	bool backToBase;

public:
	void GetDamaged(float damage);

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, 
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
			const FHitResult& Hit);

	UFUNCTION()
		void OnSensed(const TArray<AActor*>& upActors);

	void SetNewRotation(FVector target, FVector curr);
protected:
	void Dieded();

};
