// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerChar.h"
#include "Components/ActorComponent.h"
#include "WeaponComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnThrowItem);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class INCLASSSHOOTER_API UWeaponComp : public UActorComponent{
	
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponComp();

	UPROPERTY(BlueprintAssignable, Category = Interaction)
		FOnThrowItem evOnThrown;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


private:
	APlayerChar* selfActor;
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
		FVector shootPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Proyectile)
		bool bUseRaycast;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Proyectile)
		float shootSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
		float shootRange = 1000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
		int projectiles = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
		float minAccuracy = -100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
		float maxAccuracy = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
		AActor* firstOwner;


public:
	UFUNCTION(BlueprintCallable, Category=Gameplay)
		void AttachToPlayer(APlayerChar* target);

	UFUNCTION(BlueprintCallable, Category = Gameplay)
		void DisAttachToPlayer(APlayerChar* target);

	UFUNCTION(BlueprintCallable, Category=Gameplay)
		void Fire();
};
