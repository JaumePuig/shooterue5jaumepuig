// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerChar.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Math/UnrealMathUtility.h"
#include "WeaponComp.h"



// Sets default values
APlayerChar::APlayerChar()
{
	PrimaryActorTick.bCanEverTick = true;

	cam = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCam"));
	cam->SetupAttachment(GetCapsuleComponent());
	cam->bUsePawnControlRotation = true;
	

	hingeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HingeWeapon"));
	hingeMesh->SetupAttachment(cam);
	hingeMesh->bCastDynamicShadow = true;
	hingeMesh->CastShadow = false;
	// hingeMesh->SetRelativeLocation(FVector())
	hingeMesh->SetOnlyOwnerSee(true);

	UCharacterMovementComponent* cmove = GetCharacterMovement();
	cmove->BrakingFriction = 10.f;
	cmove->MaxAcceleration = 10000.f;
	cmove->MaxWalkSpeed = charMaxWalkSpeed;
	cmove->JumpZVelocity = 570.f;
	cmove->AirControl = 2.f;
	charMovement = GetCharacterMovement();
}

// Called when the game starts or when spawned
void APlayerChar::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APlayerChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (sprinting && (moveForw || moveRig)) {
		tiempoPasado += DeltaTime * 6;
	}
	else {
		tiempoPasado += DeltaTime * 2;
	}
	
	cam->PostProcessSettings.ChromaticAberrationStartOffset = FMath::Clamp(remainderf(tiempoPasado, 1.5f), 0.0f, 1.0f);

}

// Called to bind functionality to input
void APlayerChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerChar::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerChar::StopSprint);

	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &APlayerChar::OnUseItem);

	PlayerInputComponent->BindAction("ThrowWeapon", IE_Pressed, this, &APlayerChar::ThrowWeapon);
	
	PlayerInputComponent->BindAxis("Forward", this, &APlayerChar::MoveForward);
	PlayerInputComponent->BindAxis("Right", this, &APlayerChar::MoveRight);

	PlayerInputComponent->BindAxis("MouseRight", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("MouseUp", this, &APawn::AddControllerPitchInput);

}

void APlayerChar::ThrowWeapon() {
	
	if (arma) {

		UE_LOG(LogTemp, Warning, TEXT("throw weapon"));
		FDetachmentTransformRules a(EDetachmentRule::KeepWorld, false);
		arma->GetOwner()->DetachFromActor(a);
		arma->DisAttachToPlayer(this);


	}

}

void APlayerChar::Sprint() {
	sprinting = true;
	charMovement->MaxWalkSpeed = 1500.f;
}

void APlayerChar::StopSprint() {
	sprinting = false;
	charMovement->MaxWalkSpeed = 1000.f;
}

void APlayerChar::MoveForward(float vel) {

	if (sprinting) {
		charMaxWalkSpeed = 15000.f;
		if (vel != 0) {
			AddMovementInput(GetActorForwardVector(), vel * sprintMoveSpeed);
			moveForw = true;
		}else if (vel == 0) {
			moveForw = false;
		}
	}
	else {
		charMaxWalkSpeed = 1000.f;
		if (vel != 0) {
			AddMovementInput(GetActorForwardVector(), vel * moveSpeed);
			moveForw = true;
		}else if (vel == 0) {
			moveForw = false;
		}

	}
	
}

void APlayerChar::MoveRight(float vel) {
	
	if (sprinting) {
		if (vel != 0) {
			AddMovementInput(GetActorRightVector(), vel * sprintMoveSpeed);
			moveRig = true;
		}else if (vel == 0) {
			moveRig = false;
		}

	}
	else {
		if (vel != 0) {
			AddMovementInput(GetActorRightVector(), vel * moveSpeed);
			moveRig = true;
		}else if (vel == 0) {
			moveRig = false;
		}

	}

}

void APlayerChar::OnUseItem(){
	evOnUseItem.Broadcast();
}
