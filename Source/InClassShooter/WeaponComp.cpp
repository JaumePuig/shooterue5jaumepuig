// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponComp.h"
#include "EnemyChar.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UWeaponComp::UWeaponComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponComp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UWeaponComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponComp::AttachToPlayer(APlayerChar* target){

	selfActor = target;
	if(selfActor){

		target->arma = this;
		FAttachmentTransformRules rules(EAttachmentRule::SnapToTarget, true);
		GetOwner()->AttachToComponent(selfActor->GetWeaponPoint(), rules, TEXT("WeaponPoint"));
		
		selfActor->evOnUseItem.AddDynamic(this, &UWeaponComp::Fire);

	}

	
}

void UWeaponComp::DisAttachToPlayer(APlayerChar* target) {

	selfActor = target;
	if (selfActor) {

		UE_LOG(LogTemp, Warning, TEXT("disatach"));
		
		selfActor->evOnUseItem.Clear();

		selfActor->arma = NULL;

		evOnThrown.Broadcast();
		
	}

}

void UWeaponComp::Fire(){
	if(!selfActor || !selfActor->GetController())
		return;

	UE_LOG(LogTemp, Warning, TEXT("pium"));

	UWorld* const w = GetWorld();
	UCameraComponent* cam = selfActor->GetCam();
	FVector const vStart = cam->GetComponentLocation();
	
	for (int i = 0; i < projectiles; i++) {

		int x = FMath::RandRange(minAccuracy, maxAccuracy);
		int z = FMath::RandRange(minAccuracy, maxAccuracy);
		FVector const vEnd = vStart + UKismetMathLibrary::GetForwardVector(cam->GetComponentRotation()) * shootRange + FVector(x, 0, z);

		FCollisionQueryParams inParams;
		inParams.AddIgnoredActor(selfActor);

		FCollisionResponseParams outParams;

		FHitResult hit;
		w->LineTraceSingleByChannel(hit, vStart, vEnd, ECC_Camera, inParams, outParams);
		DrawDebugLine(w, vStart, vEnd, hit.bBlockingHit ? FColor::Red : FColor::Blue, false,
			5.f, 0, 10.f);
		UE_LOG(LogTemp, Warning, TEXT("Trace %s to %s"), *vStart.ToCompactString(), *vEnd.ToCompactString());

		if (hit.bBlockingHit && IsValid(hit.GetActor())) {
			UE_LOG(LogTemp, Warning, TEXT("Golpié %s"), *(hit.GetActor()->GetName()));

			AEnemyChar* e = Cast<AEnemyChar>(hit.GetActor());

			if (e) {

				e->GetDamaged(1.f);

			}

		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No >:("));
		}

	}

	//FCollisionQueryParams inParams;
	//inParams.AddIgnoredActor(selfActor);

	//FCollisionResponseParams outParams;	

	//FHitResult hit;
	//w->LineTraceSingleByChannel(hit , vStart, vEnd, ECC_Camera, inParams, outParams);
	//DrawDebugLine(w, vStart, vEnd, hit.bBlockingHit ? FColor::Red : FColor::Blue, false,
	//	5.f, 0, 10.f);
	//UE_LOG(LogTemp, Warning, TEXT("Trace %s to %s"), *vStart.ToCompactString(), *vEnd.ToCompactString());

	//if(hit.bBlockingHit && IsValid(hit.GetActor())){
	//	UE_LOG(LogTemp, Warning, TEXT("Golpié %s"), *(hit.GetActor()->GetName()));

	//	AEnemyChar* e = Cast<AEnemyChar>(hit.GetActor());

	//	if (e) {

	//		e->GetDamaged(1.f);

	//	}

	//	
	//	// e->Hit(x);
	//}else {
	//	UE_LOG(LogTemp, Warning, TEXT("No >:("));
	//}
	
}


