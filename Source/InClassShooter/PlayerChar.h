// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "PlayerChar.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUseItem);

UCLASS()
class INCLASSSHOOTER_API APlayerChar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerChar();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerVals)
		UCameraComponent* cam;
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		float moveSpeed = 5;
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		float sprintMoveSpeed = 20;
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		float charMaxWalkSpeed = 1000.f;
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		bool sprinting = false;
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		bool moveForw = false;
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		bool moveRig = false;
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		UCharacterMovementComponent* charMovement;
	UPROPERTY(EditAnywhere, Category = PlayerVals)
		class UWeaponComp* arma;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerVals)
		UStaticMeshComponent* hingeMesh;

	UPROPERTY(BlueprintAssignable, Category = PlayerVals)
		FOnUseItem evOnUseItem;

		float tiempoPasado = 0;
	
protected:
	void MoveForward(float vel);

	void MoveRight(float vel);

	void Sprint();

	void StopSprint();

	void OnUseItem();

	void ThrowWeapon();

public:
	UStaticMeshComponent* GetWeaponPoint() const { return hingeMesh ; };
	UCameraComponent* GetCam() const { return cam; }; 
};
